
### NewWorld

#### 项目介绍
使用vue-temp框架建立一个简单的阶段计划系统
#### Build Setup
```
# Clone project
git clone https://github.com/PanJiaChen/vueAdmin-template.git

# Install dependencies
npm install

# 建议不要用cnpm  安装有各种诡异的bug 可以通过如下操作解决npm速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# serve with hot reload at localhost:9528
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
#### 软件架构
软件架构说明，使用B/S的架构模式


#### 使用说明
使用Chrome浏览器打开访问

#### 参考
 [vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)

 [electron-vue-admin](https://github.com/PanJiaChen/electron-vue-admin)

 [线上模板地址](http://panjiachen.github.io/vueAdmin-template)
