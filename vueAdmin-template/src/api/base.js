import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/getBaseInfo',
    method: 'get',
    params
  })
}
export function createOrUpdate(trainfoForm) {
  return request({
    url: '/createOrUpdate',
    method: 'post',
    trainfoForm
  })
}
export function getById(id) {
  return request({
    url: '/getById',
    method: 'post',
    id
  })
}
export function deleteById(id) {
  return request({
    url: '/deleteById',
    method: 'post',
    id
  })
}
export function importBaseInfo(file) {
  return request({
    url: '/importBaseInfo',
    method: 'post',
    file
  })
}
