import request from '@/utils/request'

export function fetchList(params) {
  return request({
    //url: '/plan/Index',
    url: '/plan/getPlanInfo',
    method: 'get',
    params
  })
}
export function createOrUpdate(trainfoForm) {
  return request({
    url: '/plan/createOrUpdate',
    method: 'post',
    trainfoForm
  })
}
export function getById(id) {
  return request({
    url: '/plan/getById',
    method: 'get',
    id
  })
}
export function deleteById(id) {
  return request({
    url: '/plan/deleteById',
    method: 'post',
    id
  })
}
export function make(datetime) {
  return request({
    url: '/plan/index',
    method: 'get',
    datetime
  })
}
export function handleMake() {
  return request({
    url: '/plan/import',
    method: 'post'  
  })
}